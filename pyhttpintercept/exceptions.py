# encoding: utf-8


class NoHandlersFound(Exception):
    pass


class NoActiveModifiers(Exception):
    pass


class CircularReference(Exception):
    pass


class SSLError(Exception):
    pass
