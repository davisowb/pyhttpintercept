# encoding: utf-8

import os
import sys
import logging_helper
from configurationutil import Configuration, cfg_params
from pyhttpintercept import ServerRootWindow, __version__, __authorshort__, __module_name__

# Register Config details (These are expected to be overwritten by an importing app)
cfg_params.APP_NAME = __module_name__
cfg_params.APP_AUTHOR = __authorshort__
cfg_params.APP_VERSION = __version__
Configuration()  # Force config load!

logging = logging_helper.setup_logging(level=logging_helper.INFO,
                                       log_to_file=False)

http_ports = [8080] if sys.platform == u'darwin' and os.geteuid() != 0 else [80]
https_ports = [8443] if sys.platform == u'darwin' and os.geteuid() != 0 else [443]

ServerRootWindow(server_kwargs={u'http_ports': http_ports,
                                u'https_ports': https_ports})

# TODO: Look into https://github.com/Dejvino/https-multiplexer/blob/master/multiplexer.py for HTTPS!
